﻿using Microsoft.AspNetCore.SignalR;
using Signalr.Models;

namespace SignalR.Hubs;

public class GameHub : Hub
{
    private IBroadcaster _broadcaster;

    public GameHub(IBroadcaster broadcaster)
    {
        _broadcaster = broadcaster;
        _broadcaster.BroadcastShape(this);
        Console.WriteLine("AAAAAAAAAAAAAAA");
    }
    
    public void UpdateShape(SyncModel clientModel)
    {
        clientModel.Authority = Context.ConnectionId;
        _broadcaster.UpdateModel(clientModel);
    }

    public PlayerModel RegisterPlayer(PlayerModel player)
    {
        player.ConnectionId = Context.ConnectionId;
        _broadcaster.AddPlayer(player);

        return player;
    }
}