﻿using System.Collections.Concurrent;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.SignalR;
using SignalR.Hubs;
using Signalr.Models;

public class Broadcaster : IBroadcaster
{
    private static readonly Broadcaster _instance;

    // We're going to broadcast to all clients a maximum of 25 times per second
    private readonly TimeSpan BroadcastInterval = TimeSpan.FromMilliseconds(40);
    private readonly IHubContext<GameHub> _hubContext;
    private Timer _broadcastLoop;
    private bool _modelUpdated;
    private ConcurrentQueue<SyncModel> _oneFrameSyncModels = new ConcurrentQueue<SyncModel>();
    private ConcurrentQueue<PlayerModel> _players = new ConcurrentQueue<PlayerModel>();

    public Broadcaster(IHubContext<GameHub> hubContext)
    {
        // Save our hub context so we can easily use it 
        // to send to its connected clients
        _hubContext = hubContext;
        _modelUpdated = false;
        // Start the broadcast loop
        _broadcastLoop = new Timer(
            BroadcastShape,
            null,
            BroadcastInterval,
            BroadcastInterval);
    }

    public void AddPlayer(PlayerModel player)
    {
        int playersCount = _players.Count;
        player.PlayerIndex = playersCount;
        _players.Enqueue(player);
    }

    public void BroadcastShape(object state)
    {
        // No need to send anything if our model hasn't changed

        // This is how we can access the Clients property 
        // in a static hub method or outside of the hub entirely
        foreach (SyncModel model in _oneFrameSyncModels)
        {
            if (_players.Any(x => x.ConnectionId == model.Authority))
            {
                _hubContext.Clients.AllExcept(model.Authority).SendAsync("SendMessage", model);
            }
        }

        _oneFrameSyncModels = new ConcurrentQueue<SyncModel>();
    }

    public void UpdateModel(SyncModel clientModel)
    {
        _oneFrameSyncModels.Enqueue(clientModel);
    }
}

public interface IBroadcaster
{
    void BroadcastShape(object state);
    void UpdateModel(SyncModel clientModel);
    void AddPlayer(PlayerModel player);
}