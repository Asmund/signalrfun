using Signalr.Data;
using SignalR.Hubs;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddSingleton<WeatherForecastService>();
builder.Services.AddSingleton<Broadcaster>();
builder.Services.AddHttpClient();
builder.Services.AddSignalR();
builder.Services.AddScoped<IBroadcaster, Broadcaster>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");
app.MapHub<MyChatHub>("/mychathub");
app.MapHub<GameHub>("/mygamehub");
app.MapPost("/sendcommand", async (string message) =>
{
    var t = message;
});

app.Run();