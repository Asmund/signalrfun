﻿namespace Signalr.Models;

public class SyncModel
{
    public string Id { get; set; }
    public string Authority { get; set; }
    public float X { get; set; }
    public float Y { get; set; }
    public float Z { get; set; }
}