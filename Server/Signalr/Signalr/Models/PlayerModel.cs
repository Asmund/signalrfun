﻿namespace Signalr.Models;

public class PlayerModel
{
    public string ConnectionId;
    public int PlayerIndex;
}