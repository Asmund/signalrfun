using UnityEngine;

namespace Network
{
    public class SignalrIdentity : MonoBehaviour
    {
        [SerializeField] private bool isAuthority;
        private int _networkId;
        private string _playerName;

        public bool IsAuthority
        {
            get => isAuthority;
            set => isAuthority = value;
        }

        public int NetworkId
        {
            get => _networkId;
            set => _networkId = value;
        }

        public string PlayerName
        {
            get => _playerName;
            set => _playerName = value;
        }
    }
}
